package com.importadora.app.models;

import java.io.Serializable;

public class Session implements Serializable {
    private boolean auth;
    private String tokenType;
    private String token;

    public Session(boolean auth, String tokenType, String token) {
        this.auth = auth;
        this.tokenType = tokenType;
        this.token = token;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
