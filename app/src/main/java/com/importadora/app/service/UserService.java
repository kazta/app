package com.importadora.app.service;

import com.importadora.app.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

import static com.importadora.app.utils.Utils.constants.requestProperties.AUTHORIZATION;

public interface UserService {
    String ROUTE = "users";

    @GET(ROUTE)
    Call<List<User>> getUsers(@Header(AUTHORIZATION) String token);

    @PUT(ROUTE + "/{id}")
    Call<User> updateUser(@Path("id") int id,
                          @Header(AUTHORIZATION) String token,
                          @Body User user);

    @POST(ROUTE)
    Call<User> createUser(@Header(AUTHORIZATION) String token,
                          @Body User user);

    @DELETE(ROUTE)
    Call<User> deleteUser(@Header(AUTHORIZATION) String token,
                          @Body User user);
}
