package com.importadora.app.service;

import com.importadora.app.models.Session;
import com.importadora.app.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AuthService {
    String ROUTE = "auth/login";

    @FormUrlEncoded
    @POST(ROUTE)
    Call<Session> login(@Field("email") String email,
                        @Field("password") String password);
}
