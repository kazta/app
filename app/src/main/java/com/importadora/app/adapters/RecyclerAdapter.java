package com.importadora.app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.importadora.app.R;
import com.importadora.app.models.User;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> implements View.OnClickListener, Filterable {

    private View.OnClickListener listener;

    private List<User> data;
    private List<User> dataFull;
    private Context context;

    public RecyclerAdapter(List<User> data, Context context) {
        this.data = new ArrayList<>(data);
        this.dataFull = data;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_layout, parent, false);
        view.setOnClickListener(this);
        return new RecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(data.get(position).getName());
        holder.role.setText(data.get(position).getRole().getName());
        if (!data.get(position).getActive().equals("1"))
            holder.person.setColorFilter(ContextCompat.getColor(context, R.color.inactiveUser), android.graphics.PorterDuff.Mode.SRC_IN);
        else
            holder.person.setColorFilter(ContextCompat.getColor(context, R.color.activeUser), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView role;
        private ImageView person;

        ViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.tv_name);
            role = v.findViewById(R.id.tv_role);
            person = v.findViewById(R.id.iv_status);
        }
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<User> filteredData = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredData = dataFull;
            } else {
                String filterPattern = constraint.toString().toLowerCase();
                for (User user : dataFull) {
                    if(user.getName().toLowerCase().contains(filterPattern)){
                        filteredData.add(user);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredData;
            results.count = filteredData.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };

    public void notifyDataCanged(){
        data.clear();
        data.addAll(dataFull);
        notifyDataSetChanged();
    }

}
