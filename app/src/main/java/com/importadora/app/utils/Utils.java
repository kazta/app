package com.importadora.app.utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Debug;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.importadora.app.utils.Utils.constants.BASE_URL;
import static com.importadora.app.utils.Utils.constants.requestProperties.ACCEPT;
import static com.importadora.app.utils.Utils.constants.requestProperties.APPLICATION_JSON;
import static com.importadora.app.utils.Utils.constants.requestProperties.AUTHORIZATION;
import static com.importadora.app.utils.Utils.constants.requestProperties.CONTENT_TYPE;

public class Utils {
    public static void messageDialog(Context context, String message, String title) {
        new AlertDialog.Builder(context).setTitle(title == null ? "Information" : title)
                .setMessage(message)
                .setPositiveButton("Aceptar", null)
                .show();
    }

    public static Retrofit getRetrofit() {
        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static class constants {
        public static class requestProperties {
            public static final String AUTHORIZATION = "Authorization";
            public static final String ACCEPT = "Accept";
            public static final String APPLICATION_JSON = "application/json";
            public static final String CONTENT_TYPE = "Content-Type";
        }
        public static final String BASE_URL = "http:/192.168.1.60/api/";
        public static final String USER = "user";

    }

    @SuppressLint("DefaultLocale")
    public static String validate(@NonNull String validations, String validate) throws Exception {
        String[] validation = validations.split("\\|");
        for (String val : validation) {
            switch (val) {
                case "req":
                    if (validate == null || validate.isEmpty())
                        return "El campo es requerido";
                    break;
                case "numeric":
                    if (!isNumeric(validate))
                        return "El campo debe ser un número";
                    break;
                default:
                    if (val.contains("lenght")) {
                        int size = Integer.parseInt(val.substring(6));
                        if (validate.length() != size)
                            return String.format("El campo debe contener %d digitos", size);
                    } else if (val.contains("max")) {
                        int size = Integer.parseInt(val.substring(3));
                        if (validate.length() > size)
                            return String.format("El campo debe contener maximo %d digitos", size);
                    } else if (val.contains("min")) {
                        int size = Integer.parseInt(val.substring(3));
                        if (validate.length() < size)
                            return String.format("El campo debe contener al menos %d digitos", size);
                    } else
                        throw new Exception(String.format("%s es un criterio de validación invalido", val));
            }
        }
        return null;
    }

    private static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
