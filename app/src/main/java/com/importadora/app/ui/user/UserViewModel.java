package com.importadora.app.ui.user;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.importadora.app.models.Role;
import com.importadora.app.models.Session;
import com.importadora.app.models.User;
import com.importadora.app.repositories.UserRepository;
import com.importadora.app.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserViewModel extends ViewModel {

    private Session session;

    private MutableLiveData<List<User>> users = new MutableLiveData<>();
    private MutableLiveData<User> userListener = new MutableLiveData<>();
    private MutableLiveData<User> user = new MutableLiveData<>();
    UserRepository repository = new UserRepository();

    void init() {
        repository.getUsers(session.getTokenType() + " " + session.getToken(), users);
    }

    void processUser() {
        if (user.getValue().getId() == 0)
            repository.createUser(session.getTokenType() + " " + session.getToken(), userListener, user.getValue());
        else
            repository.updateUser(session.getTokenType()+ " " + session.getToken(), userListener, user.getValue());
    }

    LiveData<List<User>> getUsers() {
        return users;
    }

    public void setUser(User user) {
        this.user.setValue(user);
    }

    LiveData<User> getUser() {
        return user;
    }

    LiveData<User> getUserListener() {
        return userListener;
    }

    void setSession(Session session) {
        this.session = session;
    }
}
