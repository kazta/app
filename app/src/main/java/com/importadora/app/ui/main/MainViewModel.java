package com.importadora.app.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.importadora.app.models.Session;
import com.importadora.app.repositories.AuthRepository;

public class MainViewModel extends ViewModel {

    private String email;
    private String password;
    private MutableLiveData<Session> session = new MutableLiveData<>();

    public String getEmail() {
        return email;
    }

    void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    void setPassword(String password) {
        this.password = password;
    }

    void login() throws Exception {
        if (email != null && !email.isEmpty() && password != null && !password.isEmpty()) {
            new AuthRepository().login(email, password, session);
        }else
            throw new Exception("Email or password empty");
    }

    LiveData<Session> getSession(){
        return session;
    }
}
