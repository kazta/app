package com.importadora.app.ui.user;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.importadora.app.R;
import com.importadora.app.adapters.RecyclerAdapter;
import com.importadora.app.models.Session;
import com.importadora.app.models.User;

import java.util.ArrayList;
import java.util.List;

import static com.importadora.app.utils.Utils.constants.USER;

public class UserFrament extends Fragment {

    private UserViewModel mViewModel;
    private List<User> users = new ArrayList<>();
    private RecyclerAdapter adapter;
    private Session session;

    public static UserFrament newInstance() {
        return new UserFrament();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.user_fragment, container, false);
        RecyclerView recycler = view.findViewById(R.id.rv_users);
        SearchView search = view.findViewById(R.id.et_search);
        ImageButton btnAdd = view.findViewById(R.id.btn_add_user);

        adapter = new RecyclerAdapter(users, getContext());
        adapter.setOnClickListener(v -> {
            Bundle data = new Bundle();
            data.putSerializable(USER, users.get(recycler.getChildAdapterPosition(v)));
            DialogFragment dialogFrgament = new UserDialogFrafment();
            dialogFrgament.setArguments(data);
            dialogFrgament.show(getParentFragmentManager(), null);
        });

        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(adapter);

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        btnAdd.setOnClickListener(v -> new UserDialogFrafment().show(getParentFragmentManager(), null));

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            session = (Session) getArguments().getSerializable("session");
        mViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);
        // TODO: Use the ViewModel
        mViewModel.setSession(session);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel.getUsers().observe(getViewLifecycleOwner(), users -> {
            if (!this.users.isEmpty())
                this.users.clear();
            this.users.addAll(users);
            adapter.notifyDataCanged();
        });
        mViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            mViewModel.processUser();
        });
        mViewModel.getUserListener().observe(getViewLifecycleOwner(), user->{
            mViewModel.init();
        });
        mViewModel.init();
    }
}
