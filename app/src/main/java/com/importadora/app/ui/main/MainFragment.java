package com.importadora.app.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.importadora.app.R;
import com.importadora.app.ui.user.UserFrament;
import com.importadora.app.utils.Utils;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        Button login = view.findViewById(R.id.btn_login);
        final TextView email = view.findViewById(R.id.email);
        final TextView password = view.findViewById(R.id.password);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.setEmail(email.getText().toString());
                mViewModel.setPassword(password.getText().toString());
                try {
                    mViewModel.login();
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.messageDialog(getContext(), e.getMessage(), "Error");
                }
            }
        });
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel.getSession().observe(getViewLifecycleOwner(), session -> {
            if(session.isAuth()){
                Fragment fragment = UserFrament.newInstance();
                Bundle data = new Bundle();
                data.putSerializable("session",session);
                fragment.setArguments(data);
                getParentFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
            }
            else{
                Utils.messageDialog(getContext(),session.getTokenType() + session.getToken(),null);
            }
        });
    }
}
