package com.importadora.app.ui.user;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.importadora.app.R;
import com.importadora.app.models.Role;
import com.importadora.app.models.User;

import static com.importadora.app.utils.Utils.constants.USER;

public class UserDialogFrafment extends DialogFragment {

    User user;
    boolean add = true;
    UserViewModel mViewModel;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.user_dialog, null, false);

        TextView labelPassword = view.findViewById(R.id.label_password);
        Spinner active = view.findViewById(R.id.active);
        Spinner role = view.findViewById(R.id.role);
        EditText password = view.findViewById(R.id.password);
        EditText email = view.findViewById(R.id.email);
        EditText name = view.findViewById(R.id.name);

        ImageButton btnDelete = view.findViewById(R.id.btn_delete);
        Button btnSave = view.findViewById(R.id.btn_save);


        ArrayAdapter<CharSequence> activeAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.active_options, android.R.layout.simple_spinner_item);
        activeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        active.setAdapter(activeAdapter);

        ArrayAdapter<CharSequence> roleAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.role_options, android.R.layout.simple_spinner_item);
        roleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        role.setAdapter(roleAdapter);

        if (getArguments() != null) {
            user = (User) getArguments().getSerializable(USER);
            labelPassword.setVisibility(View.GONE);
            password.setVisibility(View.GONE);
            btnSave.setText("Editar");
            active.setSelection(Integer.parseInt(user.getActive()) - 1);
            active.setEnabled(false);
            role.setSelection(user.getRole().getId() - 1);
            role.setEnabled(false);
            email.setText(user.getEmail());
            email.setEnabled(false);
            name.setText(user.getName());
            name.setEnabled(false);
            add = false;
        }

        btnSave.setOnClickListener(v -> {
            if (add) {
                if (user == null) {
                    user = new User();
                    user.setRole(new Role());
                }

                user.setPassword(password.getText().toString());
                user.setActive(String.valueOf(active.getSelectedItemPosition() + 1));
                user.setRoleId(role.getSelectedItemPosition() + 1);
                user.setEmail(email.getText().toString());
                user.setName(name.getText().toString());

                mViewModel.setUser(user);

                dismiss();

            } else {
                labelPassword.setVisibility(View.VISIBLE);
                password.setVisibility(View.VISIBLE);
                btnSave.setText("Guardar");
                active.setEnabled(true);
                role.setEnabled(true);
                email.setEnabled(true);
                name.setEnabled(true);

                add = true;
            }
        });

        builder.setView(view);

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);
    }
}
