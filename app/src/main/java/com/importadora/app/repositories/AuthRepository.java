package com.importadora.app.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.importadora.app.models.Session;
import com.importadora.app.service.AuthService;
import com.importadora.app.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthRepository {
    public void login(String email, String password, MutableLiveData<Session> session) {

        AuthService service = Utils.getRetrofit().create(AuthService.class);
        Call<Session> call = service.login(email, password);

        call.enqueue(new Callback<Session>() {
            @Override
            public void onResponse(Call<Session> call, Response<Session> response) {
                if (response.body() != null) {
                    session.setValue(response.body());
                } else {
                    session.setValue(new Session(false,String.valueOf(response.code()), response.message()));
                }
            }

            @Override
            public void onFailure(Call<Session> call, Throwable t) {
                Log.i("Respose", t.getMessage());
            }
        });
    }
}
