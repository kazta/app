package com.importadora.app.repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.importadora.app.models.Session;
import com.importadora.app.models.User;
import com.importadora.app.service.AuthService;
import com.importadora.app.service.UserService;
import com.importadora.app.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {
    public void getUsers(String token, MutableLiveData<List<User>> users) {
        UserService service = Utils.getRetrofit().create(UserService.class);
        Call<List<User>> call = service.getUsers(token);
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.body() != null) {
                    users.setValue(response.body());
                } else {
                    //TODO: Implementar manejo de errores
                    Log.i("GetUsers", "Error");
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.i("Respose", t.getMessage());
            }
        });
    }

    public void updateUser(String token, MutableLiveData<User> userListener, User user) {
        UserService service = Utils.getRetrofit().create(UserService.class);
        Call<User> call = service.updateUser(user.getId(), token, user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null) {
                    userListener.setValue(response.body());
                } else {
                    //TODO: Implementar manejo de errores
                    Log.i("GetUsers", "Error");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.i("Respose", t.getMessage());
            }
        });
    }

    public void createUser(String token, MutableLiveData<User> userListener, User user) {
        UserService service = Utils.getRetrofit().create(UserService.class);
        Call<User> call = service.createUser(token, user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null) {
                    userListener.setValue(response.body());
                } else {
                    //TODO: Implementar manejo de errores
                    Log.i("GetUsers","Error");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.i("Respose", t.getMessage());
            }
        });
    }
}

